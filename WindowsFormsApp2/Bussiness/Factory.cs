﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public class Factory<T>
    {


        public Factory()
        {

        }

        public List<TextBox> createTextboxes()
        {

            List<TextBox> txBoxes = new List<TextBox>();
            for (int i = 0; i < typeof(T).GetProperties().Length; i++)
            {
                TextBox aux = new TextBox();
                aux.Location = new System.Drawing.Point(100 * i, 400);
                aux.Text ="";
                txBoxes.Add(aux);
                aux.Visible = true;
            }
            return txBoxes;
        }


        DataTable populateTable(DataTable dt, List<T> items)
        {
            foreach (var f in items)
            {
                DataRow dr = dt.NewRow();
                //populate table
                var fields = typeof(T).GetProperties();
                foreach (var t in fields)
                {
                    dr[t.Name] = typeof(T).GetProperty(t.Name).GetValue(f);
                }
                dt.Rows.Add(dr);


            }
            return dt;

        }
        public void WriteToCsv<T>(List<T> list)
            {
            //before your loop
                var csv = new StringBuilder();

            using (var w = new StreamWriter(@"D:\\test.csv"))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string comma = "";
                    StringBuilder line = new StringBuilder("");
                   foreach(var f in typeof(T).GetFields())
                    {
                        line.Append(comma + f.Name);
                        comma = ",";
                        line.Append(comma + f.GetValue(list[i]));
                        
                     
                    }
                   
                    w.WriteLine(line.ToString());
                    w.Flush();

                }
                w.Flush();
                w.Close();
            }
            
            

        }
        public DataTable createTable(List<T> items)
        {
            DataTable tab = new DataTable();
           foreach (var f in typeof(T).GetProperties())
            {
                tab.Columns.Add(new DataColumn(f.Name, Nullable.GetUnderlyingType(
            f.PropertyType) ?? f.PropertyType));

            }
           

            return populateTable(tab,items);
        }
    }
}
